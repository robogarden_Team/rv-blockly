Blockly.Python['moveOneCell'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.Python.startHighlight_(block)+'Robo.stepForward() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};
Blockly.Python['moveOneCell_'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.Python.startHighlight_(block)+'Robo.stepBackward() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['moveCircle'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'R',
            Blockly.Python.ORDER_NONE) || '\'\'';

    var code =  Blockly.Python.startHighlight_(block)+'Robo.moveInArc('+ argument0+','+'360) \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['rotate_SL'] = function(block) {
    // Search the text for a substring.

    var code =  Blockly.Python.startHighlight_(block)+'Robo.rotateLeft() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['rotate_SR'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.Python.startHighlight_(block)+'Robo.rotateRight() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['enable_general'] = function(block) {
    var port = this.getFieldValue('po');
    var peri = this.getFieldValue('pe');
    var code ='enable('+peri+','+port+') \r\n';
    return code;
};

Blockly.Python['enable_pilot'] = function(block) {
    var code ='enablePilot() \r\n';
    return code;
};

Blockly.Python['getColor_B'] = function(block) {
    var code ='Robo.getCellColor()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['getRemote_B'] = function(block) {
    var mode = this.getFieldValue('CH');
    var code ='getRemoteCommand('+mode+')';
    return [code,Blockly.Python.ORDER_ATOMIC];
    //return 'tarek';
};

Blockly.Python['getDis_IR_B'] = function(block) {
    var code ='Robo.getDistanceIR()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};
Blockly.Python['getDis_US_B'] = function(block) {
    var code ='Robo.getDistanceUltraSonic()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['getLightI_B'] = function(block) {
    var code ='getLIntensity()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['getTouch_B'] = function(block) {
    var code ='getTouchLevel()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['getNoise_B'] = function(block) {
    var code ='getSoundDb()';
    return [code,Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['GripOpen'] = function(block) {
    var code ='GripOpen() \r\n';
    return code;
};

Blockly.Python['GripClose'] = function(block) {
    var code ='GripClose() \r\n';
    return code;
};

Blockly.Python['hammerUP'] = function(block) {
    var code ='hammerUP() \r\n';
    return code;
};

Blockly.Python['hammerdown'] = function(block) {
    var code ='hammerdown() \r\n';
    return code;
};

Blockly.Python['throwBall'] = function(block) {
    var code ='throwBall() \r\n';
    return code;
};

Blockly.Python['rotate_fan'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'A',
            Blockly.Python.ORDER_NONE) || '\'\'';

    var code =  'rotateFan('+ argument0+') \r\n';
    return code;
};
Blockly.Python['rotate_fan_to'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'A',
            Blockly.Python.ORDER_NONE) || '\'\'';

    var code =  'rotateFanToAngle('+ argument0+') \r\n';
    return code;
};

Blockly.Python['movArc'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'R',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var argument1 = Blockly.Python.valueToCode(block, 'A',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var code =  'Robo.moveInArc('+ argument0+','+argument1+') \r\n';
    return code;
};

Blockly.Python['movArcBackward'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'R',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var code =  'movArcBackward('+ argument0+') \r\n';
    return code;
};

Blockly.Python['movArcForward'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'R',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var code =  'movArcForward('+ argument0+') \r\n';
    return code;
};

Blockly.Python['moveBackward'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.Python.startHighlight_(block)+'Robo.movBackward() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['moveForward'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.Python.startHighlight_(block)+'Robo.movForward() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['rotate'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'A',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var code =  Blockly.Python.startHighlight_(block)+'Robo.rotate('+ argument0+') \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['rotateClockwise'] = function(block) {
    // Search the text for a substring.
    var code ='rotateCW() \r\n';
    return code;
};

Blockly.Python['rotateCounterclockwise'] = function(block) {
    // Search the text for a substring.
   var code ='rotateCCW() \r\n';
    return code;
};

Blockly.Python['stop'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.Python.startHighlight_(block)+'Robo.stop() \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['travel'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.Python.valueToCode(block, 'D',
            Blockly.Python.ORDER_NONE) || '\'\'';
    var code =  Blockly.Python.startHighlight_(block)+'Robo.travel('+ argument0+') \r\n'+Blockly.Python.endHighlight_(block);
    return code;
};

Blockly.Python['black'] = function(block) {
    var code = Blockly.Python.quote_('black');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['red'] = function(block) {
    var code = Blockly.Python.quote_('red');
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['white'] = function(block) {
    var code = Blockly.Python.quote_('white');
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['green'] = function(block) {
    var code = Blockly.Python.quote_('green');
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['blue'] = function(block) {
    var code = Blockly.Python.quote_('blue');
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['brown'] = function(block) {
    var code = Blockly.Python.quote_('brown');
    return [code, Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python['yellow'] = function(block) {
    var code = Blockly.Python.quote_('yellow');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['cracked_ice'] = function(block) {
    var code = Blockly.Python.quote_('cracked_ice');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['thin_ice'] = function(block) {
    var code = Blockly.Python.quote_('thin_ice');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['mud'] = function(block) {
    var code = Blockly.Python.quote_('mud');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['hole'] = function(block) {
    var code = Blockly.Python.quote_('hole');
    return [code, Blockly.Python.ORDER_ATOMIC];

};

Blockly.Python['paint_cell_with_color'] = function(block) {
  var value_color = Blockly.Python.valueToCode(block, 'color', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.startHighlight_(block)+'Robo.setCellColor(' + value_color + ') \r\n'+Blockly.Python.endHighlight_(block);
  return code;
};

Blockly.Python['print_to_cell'] = function(block) {
  var value_text = Blockly.Python.valueToCode(block, 'text', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.startHighlight_(block)+'Robo.setCellText(' +  value_text +') \r\n'+Blockly.Python.endHighlight_(block);
  return code;
};


Blockly.Python['wait_for_seconds'] = function(block) {
    debugger;
  var value_seconds = Blockly.Python.valueToCode(block, 'seconds', Blockly.Python.ORDER_NONE);
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.startHighlight_(block)+'Robo.delay(' + value_seconds +') \r\n'+Blockly.Python.endHighlight_(block);
  return code;
};

Blockly.Python['set_auto_color'] = function(block) {
  var value_color = Blockly.Python.valueToCode(block, 'color', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.startHighlight_(block)+'Robo.setAutoColor(' + value_color + ') \r\n'+Blockly.Python.endHighlight_(block);
  return code;
};

Blockly.Python['drop_item'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.startHighlight_(block)+'Robo.dropObject() \r\n'+Blockly.Python.endHighlight_(block);
  return code;
};

Blockly.Python['read_from_cell'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = 'Robo.getCellText() \r\n';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['read_number_from_cell'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = 'Robo.getCellNumber() \r\n';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['none_color'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = Blockly.Python.quote_('none');
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['move_forward_then_return'] = function(block) {

    var code = 'Robo.stepForward()\r\n';
    code += 'Robo.stepBackward()\r\n';
    return Blockly.Python.startHighlight_(block)+code+Blockly.Python.endHighlight_(block);
};

Blockly.Python['move_backward_then_return'] = function(block) {

    var code = 'Robo.rotateRight()\r\n';
    code += 'Robo.rotateRight()\r\n';
    code += 'Robo.stepForward()\r\n';
    code += 'Robo.rotateRight()\r\n';
    code += 'Robo.rotateRight()\r\n';
    code += 'Robo.stepForward()\r\n';
    return Blockly.Python.startHighlight_(block)+code+Blockly.Python.endHighlight_(block);
};

Blockly.Python['move_right_then_return'] = function(block) {

    var code = 'Robo.rotateRight()\r\n';
    code += 'Robo.stepForward()\r\n';
    code += 'Robo.rotateLeft()\r\n';
    code += 'Robo.rotateLeft()\r\n';
    code += 'Robo.stepForward()\r\n';
    code += 'Robo.rotateRight()\r\n';
    return Blockly.Python.startHighlight_(block)+code+Blockly.Python.endHighlight_(block);
};

Blockly.Python['move_left_then_return'] = function(block) {

    var code = 'Robo.rotateLeft()\r\n';
    code += 'Robo.stepForward()\r\n';
    code += 'Robo.rotateRight()\r\n';
    code += 'Robo.rotateRight()\r\n';
    code += 'Robo.stepForward()\r\n';
    code += 'Robo.rotateLeft()\r\n';
    return Blockly.Python.startHighlight_(block)+code+Blockly.Python.endHighlight_(block);
};

Blockly.Python.zero=function(a){return[Blockly.Python.quote_("0"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.one=function(a){return[Blockly.Python.quote_("1"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.two=function(a){return[Blockly.Python.quote_("2"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.three=function(a){return[Blockly.Python.quote_("3"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.four=function(a){return[Blockly.Python.quote_("4"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.five=function(a){return[Blockly.Python.quote_("5"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.six=function(a){return[Blockly.Python.quote_("6"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.seven=function(a){return[Blockly.Python.quote_("7"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.eight=function(a){return[Blockly.Python.quote_("8"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.nine=function(a){return[Blockly.Python.quote_("9"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.ten=function(a){return[Blockly.Python.quote_("10"),Blockly.Python.ORDER_ATOMIC]};

Blockly.Python.a=function(a){return[Blockly.Python.quote_("A"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.b=function(a){return[Blockly.Python.quote_("B"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.c=function(a){return[Blockly.Python.quote_("C"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.d=function(a){return[Blockly.Python.quote_("D"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.e=function(a){return[Blockly.Python.quote_("E"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.f=function(a){return[Blockly.Python.quote_("F"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.g=function(a){return[Blockly.Python.quote_("G"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.h=function(a){return[Blockly.Python.quote_("H"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.i=function(a){return[Blockly.Python.quote_("I"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.j=function(a){return[Blockly.Python.quote_("J"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.k=function(a){return[Blockly.Python.quote_("K"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.l=function(a){return[Blockly.Python.quote_("L"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.m=function(a){return[Blockly.Python.quote_("M"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.n=function(a){return[Blockly.Python.quote_("N"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.o=function(a){return[Blockly.Python.quote_("O"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.p=function(a){return[Blockly.Python.quote_("P"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.q=function(a){return[Blockly.Python.quote_("Q"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.r=function(a){return[Blockly.Python.quote_("R"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.s=function(a){return[Blockly.Python.quote_("S"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.t=function(a){return[Blockly.Python.quote_("T"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.u=function(a){return[Blockly.Python.quote_("U"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.v=function(a){return[Blockly.Python.quote_("V"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.w=function(a){return[Blockly.Python.quote_("W"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.x=function(a){return[Blockly.Python.quote_("X"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.y=function(a){return[Blockly.Python.quote_("Y"),Blockly.Python.ORDER_ATOMIC]};
Blockly.Python.z=function(a){return[Blockly.Python.quote_("Z"),Blockly.Python.ORDER_ATOMIC]};

Blockly.Python['move_forward_and_print'] = function(block) {
    var value_printed = Blockly.Python.valueToCode(block, 'printed', Blockly.Python.ORDER_ATOMIC);
    var code ="Robo.stepForward() \r\n";
    code += "Robo.setCellText("+value_printed+")\r\n";
    return Blockly.Python.startHighlight_(block)+code+Blockly.Python.endHighlight_(block);
};