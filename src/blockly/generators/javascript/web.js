Blockly.JavaScript['alert'] = function(block) {
    var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
        Blockly.JavaScript.ORDER_FUNCTION_CALL) || '\'\'';
    var code = 'alert('+argument0+'); \r\n'; 
    return code;
};

Blockly.JavaScript['getElementById'] = function(block) {
    var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
        Blockly.JavaScript.ORDER_FUNCTION_CALL) || '\'\'';
    var code = 'document.getElementById('+argument0+'); \r\n'; 
    return code;
};