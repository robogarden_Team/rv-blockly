Blockly.JavaScript['moveOneCell'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.stepForward(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};
Blockly.JavaScript['moveOneCell_'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.stepBackward(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['moveCircle'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';

    var code =  'Robo.moveInArc('+ argument0+','+'360); \r\n';
    return code;
};

Blockly.JavaScript['rotate_SL'] = function(block) {
    // Search the text for a substring.

    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.rotateLeft(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['rotate_SR'] = function(block) {
    // Search the text for a substring.
    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.rotateRight(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['enable_general'] = function(block) {
    var port = this.getFieldValue('po');
    var peri = this.getFieldValue('pe');
    var code ='enable('+peri+','+port+') \r\n';
    return code;
};

Blockly.JavaScript['enable_pilot'] = function(block) {
    var code ='enablePilot() \r\n';
    return code;
};

Blockly.JavaScript['getColor_B'] = function(block) {
    var code = 'Robo.getCellColor()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['getRemote_B'] = function(block) {
    var mode = this.getFieldValue('CH');
    var code ='getRemoteCommand('+mode+')';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
    //return 'tarek';
};

Blockly.JavaScript['getDis_IR_B'] = function(block) {
    var code ='Robo.getDistanceIR()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};
Blockly.JavaScript['getDis_US_B'] = function(block) {
    var code ='Robo.getDistanceUltraSonic()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['getLightI_B'] = function(block) {
    var code ='getLIntensity()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['getTouch_B'] = function(block) {
    var code ='getTouchLevel()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['getNoise_B'] = function(block) {
    var code ='getSoundDb()';
    return [code,Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['GripOpen'] = function(block) {
    var code ='GripOpen() \r\n';
    return code;
};

Blockly.JavaScript['GripClose'] = function(block) {
    var code ='GripClose() \r\n';
    return code;
};

Blockly.JavaScript['hammerUP'] = function(block) {
    var code ='hammerUP() \r\n';
    return code;
};

Blockly.JavaScript['hammerdown'] = function(block) {
    var code ='hammerdown() \r\n';
    return code;
};

Blockly.JavaScript['throwBall'] = function(block) {
    var code ='throwBall() \r\n';
    return code;
};

Blockly.JavaScript['rotate_fan'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';

    var code =  'rotateFan('+ argument0+') \r\n';
    return code;
};
Blockly.JavaScript['rotate_fan_to'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';

    var code =  'rotateFanToAngle('+ argument0+') \r\n';
    return code;
};

Blockly.JavaScript['movArc'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var argument1 = Blockly.JavaScript.valueToCode(block, 'A',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var code =  'Robo.moveInArc('+ argument0+','+argument1+'); \r\n';
    return code;
};

Blockly.JavaScript['movArcBackward'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var code =  'movArcBackward('+ argument0+'); \r\n';
    return code;
};

Blockly.JavaScript['movArcForward'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'R',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var code =  'movArcForward('+ argument0+'); \r\n';
    return code;
};

Blockly.JavaScript['moveBackward'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.JavaScript.startHighlight_(block)+'Robo.movBackward(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['moveForward'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.JavaScript.startHighlight_(block)+'Robo.movForward(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['rotate'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'A',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.rotate('+ argument0+'); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['rotateClockwise'] = function(block) {
    // Search the text for a substring.
    var code ='rotateCW() \r\n';
    return code;
};

Blockly.JavaScript['rotateCounterclockwise'] = function(block) {
    // Search the text for a substring.
    var code ='rotateCCW() \r\n';
    return code;
};

Blockly.JavaScript['stop'] = function(block) {
    // Search the text for a substring.
    var code =Blockly.JavaScript.startHighlight_(block)+'Robo.stop(); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['travel'] = function(block) {
    // Search the text for a substring.
    var argument0 = Blockly.JavaScript.valueToCode(block, 'D',
            Blockly.JavaScript.ORDER_NONE) || '\'\'';
    var code =  Blockly.JavaScript.startHighlight_(block)+'Robo.travel('+ argument0+'); \r\n'+Blockly.JavaScript.endHighlight_(block);
    return code;
};

Blockly.JavaScript['black'] = function(block) {
    var code = Blockly.JavaScript.quote_('black');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['red'] = function(block) {
    var code = Blockly.JavaScript.quote_('red');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['white'] = function(block) {
    var code = Blockly.JavaScript.quote_('white');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['green'] = function(block) {
    var code = Blockly.JavaScript.quote_('green');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['blue'] = function(block) {
    var code = Blockly.JavaScript.quote_('blue');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['brown'] = function(block) {
    var code = Blockly.JavaScript.quote_('brown');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['yellow'] = function(block) {
    var code = Blockly.JavaScript.quote_('yellow');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['cracked_ice'] = function(block) {
    var code = Blockly.JavaScript.quote_('cracked_ice');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['thin_ice'] = function(block) {
    var code = Blockly.JavaScript.quote_('thin_ice');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['mud'] = function(block) {
    var code = Blockly.JavaScript.quote_('mud');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['hole'] = function(block) {
    var code = Blockly.JavaScript.quote_('hole');
    return [code, Blockly.JavaScript.ORDER_ATOMIC];

};


Blockly.JavaScript['paint_cell_with_color'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.startHighlight_(block)+'Robo.setCellColor(' + value_color + ');'+Blockly.JavaScript.endHighlight_(block);
  return code;
};

Blockly.JavaScript['print_to_cell'] = function(block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.startHighlight_(block)+'Robo.setCellText(' + value_text + ');'+Blockly.JavaScript.endHighlight_(block);
  return code;
};

Blockly.JavaScript['wait_for_seconds'] = function(block) {
  var value_seconds = Blockly.JavaScript.valueToCode(block, 'seconds', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.startHighlight_(block)+'Robo.delay(' + value_seconds + ');'+Blockly.JavaScript.endHighlight_(block);
  return code;
};

Blockly.JavaScript['set_auto_color'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.startHighlight_(block)+'Robo.setAutoColor(' + value_color + ');'+Blockly.JavaScript.endHighlight_(block);
  return code;
};

Blockly.JavaScript['drop_item'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.startHighlight_(block)+'Robo.dropObject();'+Blockly.JavaScript.endHighlight_(block);
  return code;
};

Blockly.JavaScript['read_from_cell'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'Robo.getCellText()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['read_number_from_cell'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'Robo.getCellNumber()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['none_color'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = Blockly.JavaScript.quote_('none');
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['move_forward_then_return'] = function(block) {

    var code = 'Robo.stepForward();\r\n';
    code += 'Robo.stepBackward();\r\n';
    return Blockly.JavaScript.startHighlight_(block)+code+Blockly.JavaScript.endHighlight_(block)
};

Blockly.JavaScript['move_backward_then_return'] = function(block) {

    var code = 'Robo.rotateRight();\r\n';
    code += 'Robo.rotateRight();\r\n';
    code += 'Robo.stepForward();\r\n';
    code += 'Robo.rotateRight();\r\n';
    code += 'Robo.rotateRight();\r\n';
    code += 'Robo.stepForward();\r\n';
    return Blockly.JavaScript.startHighlight_(block)+code+Blockly.JavaScript.endHighlight_(block);
};

Blockly.JavaScript['move_right_then_return'] = function(block) {

    var code = 'Robo.rotateRight();\r\n';
    code += 'Robo.stepForward();\r\n';
    code += 'Robo.rotateLeft();\r\n';
    code += 'Robo.rotateLeft();\r\n';
    code += 'Robo.stepForward();\r\n';
    code += 'Robo.rotateRight();\r\n';
    return Blockly.JavaScript.startHighlight_(block)+code+Blockly.JavaScript.endHighlight_(block);
};

Blockly.JavaScript['move_left_then_return'] = function(block) {

    var code = 'Robo.rotateLeft();\r\n';
    code += 'Robo.stepForward();\r\n';
    code += 'Robo.rotateRight();\r\n';
    code += 'Robo.rotateRight();\r\n';
    code += 'Robo.stepForward();\r\n';
    code += 'Robo.rotateLeft();\r\n';
    return Blockly.JavaScript.startHighlight_(block)+code+Blockly.JavaScript.endHighlight_(block);
};

Blockly.JavaScript.zero=function(a){return[Blockly.JavaScript.quote_("0"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.one=function(a){return[Blockly.JavaScript.quote_("1"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.two=function(a){return[Blockly.JavaScript.quote_("2"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.three=function(a){return[Blockly.JavaScript.quote_("3"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.four=function(a){return[Blockly.JavaScript.quote_("4"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.five=function(a){return[Blockly.JavaScript.quote_("5"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.six=function(a){return[Blockly.JavaScript.quote_("6"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.seven=function(a){return[Blockly.JavaScript.quote_("7"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.eight=function(a){return[Blockly.JavaScript.quote_("8"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.nine=function(a){return[Blockly.JavaScript.quote_("9"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.ten=function(a){return[Blockly.JavaScript.quote_("10"),Blockly.JavaScript.ORDER_ATOMIC]};

Blockly.JavaScript.a=function(a){return[Blockly.JavaScript.quote_("A"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.b=function(a){return[Blockly.JavaScript.quote_("B"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.c=function(a){return[Blockly.JavaScript.quote_("C"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.d=function(a){return[Blockly.JavaScript.quote_("D"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.e=function(a){return[Blockly.JavaScript.quote_("E"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.f=function(a){return[Blockly.JavaScript.quote_("F"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.g=function(a){return[Blockly.JavaScript.quote_("G"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.h=function(a){return[Blockly.JavaScript.quote_("H"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.i=function(a){return[Blockly.JavaScript.quote_("I"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.j=function(a){return[Blockly.JavaScript.quote_("J"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.k=function(a){return[Blockly.JavaScript.quote_("K"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.l=function(a){return[Blockly.JavaScript.quote_("L"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.m=function(a){return[Blockly.JavaScript.quote_("M"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.n=function(a){return[Blockly.JavaScript.quote_("N"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.o=function(a){return[Blockly.JavaScript.quote_("O"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.p=function(a){return[Blockly.JavaScript.quote_("P"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.q=function(a){return[Blockly.JavaScript.quote_("Q"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.r=function(a){return[Blockly.JavaScript.quote_("R"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.s=function(a){return[Blockly.JavaScript.quote_("S"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.t=function(a){return[Blockly.JavaScript.quote_("T"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.u=function(a){return[Blockly.JavaScript.quote_("U"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.v=function(a){return[Blockly.JavaScript.quote_("V"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.w=function(a){return[Blockly.JavaScript.quote_("W"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.x=function(a){return[Blockly.JavaScript.quote_("X"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.y=function(a){return[Blockly.JavaScript.quote_("Y"),Blockly.JavaScript.ORDER_ATOMIC]};
Blockly.JavaScript.z=function(a){return[Blockly.JavaScript.quote_("Z"),Blockly.JavaScript.ORDER_ATOMIC]};

Blockly.JavaScript['move_forward_and_print'] = function(block) {
    var value_printed = Blockly.JavaScript.valueToCode(block, 'printed', Blockly.JavaScript.ORDER_ATOMIC);
    var code ="Robo.stepForward(); \r\n";
    code += "Robo.setCellText("+value_printed+");\r\n";
    return Blockly.JavaScript.startHighlight_(block)+code+Blockly.JavaScript.endHighlight_(block);
};