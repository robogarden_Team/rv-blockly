Blockly.Blocks['alert'] = {
    init: function() {
        this.appendValueInput('VALUE')
            .setAlign(Blockly.ALIGN_CENTRE)
            .setCheck('String')
            .appendField('alert');
        this.setColour('#57BAD7');
        this.setTooltip('');
        this.setHelpUrl('');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks['getElementById'] = {
    init: function() {
        this.appendValueInput('VALUE')
            .setAlign(Blockly.ALIGN_CENTRE)
            .setCheck('String')
            .appendField('getElementById');  
        this.setColour('#57BAD7');
        this.setTooltip('');
        this.setHelpUrl('');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};