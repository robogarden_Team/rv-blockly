var movement_translation_color = "#19a1c8";
var movement_rotation_color = "#61d465";
var user_interaction_color = "#f02a76";
var color_sensor_color = "#9a57e8";
var distance_sensor_color = "#5f7d8b";
var low_level_translation_color = "#1ba8c2";
var move_forward_and_print_color = "#e33d9c";

Blockly.Blocks['moveOneCell'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(movement_translation_color);
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldImage("assets/blocks/step_forward.png", 137, 20, "step forward"));
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.MOVEONECELL_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};
Blockly.Blocks['moveOneCell_'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(movement_translation_color);
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldImage("assets/blocks/step_backward.png", 153, 20, "step backward"));
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.MOVEONECELLPREVIOUS_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['moveCircle'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(120);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MOVECIRCLE_TITLE1);
        this.appendValueInput('R');
        this.appendDummyInput()
            .appendField(Blockly.Msg.MOVECIRCLE_TITLE2);
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.MOVECIRCLE_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['rotate_SL'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(movement_rotation_color);
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldImage("assets/blocks/rotate_left.png", 115, 20, "rotate left"));
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.ROTATESL_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['rotate_SR'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(movement_rotation_color);
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldImage("assets/blocks/rotate_right.png", 128, 20, "rotate right"));
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.ROTATESR_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['enable_general'] = {
    init: function() {
        var PORTS =
            [['1', '1'],
                ['2','2'],
                ['3','3'],
                ['4','4']];
        var Peri =
            [['ColorSensor', '0'],
                ['IRSensor','1'],
                ['Motor','2'],
                ['UltraSonicSensor','3'],
                ['TouchSensor','4'],
                ['LightSensor','5'],
                ['MediumMotor','6'],
                ['SoundSensor','7'],
                ['Fan','8'],
                ['Hammer','9'],
                ['Gripper','10'],
                ['BallThrower','11']];
        this.setHelpUrl(null);
        this.setColour(0);

        this.appendDummyInput()
            .appendField('Enable')
            .appendField('Item')
            .appendField(new Blockly.FieldDropdown(Peri), 'pe')
            .appendField('On Port')
            .appendField(new Blockly.FieldDropdown(PORTS), 'po');
        this.setTooltip('enables you to capture the remote button ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks['enable_pilot'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(0);
        this.appendDummyInput()
            .appendField('Enable Pilot');
        this.setTooltip('enable The pilot');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['getColor_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);

        this.appendDummyInput()
            .appendField(new Blockly.FieldImage("assets/blocks/get_color.png", 135, 20, "get cell color"));
        this.setOutput(true, 'String');
        this.setTooltip('enables you to capture the color in front of you');
    }
};

Blockly.Blocks['getRemote_B'] = {
    init: function() {
        var OPERATORS =
            [['1', '1'],
                ['2','2'],
                ['3','3'],
                ['4','4']];
        this.setHelpUrl(null);
        this.setColour(250);

        this.appendDummyInput()
            .appendField('Get Remote Command ')
            .appendField('From Channel')
            .appendField(new Blockly.FieldDropdown(OPERATORS), 'CH');
        this.setOutput(true, 'Number');
        this.setTooltip('enables you to capture the remote button ');
    }
};

Blockly.Blocks['getDis_IR_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(260);
        this.appendDummyInput()
            .appendField('Get Distance Using IR Sensor');
        this.setOutput(true, 'Number');
        this.setTooltip('Max Ultrasonic Distance is 50m ');
    }
};

Blockly.Blocks['getDis_US_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(distance_sensor_color);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage("assets/blocks/get_distance.png", 185, 20, "get distance ahead"));
        this.setOutput(true, 'Number');
        this.setTooltip('Max Infrared Distance is 100m');
    }
};

Blockly.Blocks['getLightI_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(256);
        this.appendDummyInput()
            .appendField('Get Light Intensity');
        this.setOutput(true, 'Number');
        this.setTooltip('enables you to capture the Intensity of the light');
    }
};
Blockly.Blocks['getTouch_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(254);
        this.appendDummyInput()
            .appendField(' Is Touched');
        this.setOutput(true, 'Boolean');
        this.setTooltip('if touched return true ');
    }
};

Blockly.Blocks['getNoise_B'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(252);
        this.appendDummyInput()
            .appendField('Get Noise Level');
        this.setOutput(true, 'Number');
        this.setTooltip('return amount of noise');
    }
};

Blockly.Blocks['GripOpen'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Open Grip');
        this.setTooltip('Open the Gripper Arm');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['GripClose'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Close Grip');
        this.setTooltip('Close the Gripper Arm');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['hammerUP'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Relax Hammer');
        this.setTooltip('relax the hammer Arm');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};



Blockly.Blocks['hammerdown'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Knock with Hammer');
        this.setTooltip('Knock With hammer');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['throwBall'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Shoot');
        this.setTooltip('throw the ball ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};
Blockly.Blocks['rotate_fan'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Rotate Fan with Angle');
        this.appendValueInput('A');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('rotate fan with angle  ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};
Blockly.Blocks['rotate_fan_to'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(100);
        this.appendDummyInput()
            .appendField('Rotate Fan to Angle');
        this.appendValueInput('A');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('rotate fan to angle  ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['movArc'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(120);
        this.appendDummyInput()
            .appendField('Rotate in Arch with Radius')
        this.appendValueInput('R');
        this.appendDummyInput()
            .appendField('and Angle');
        this.appendValueInput('A');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('enables the robot to Move forward with specific radius');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['movArcBackward'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(210);
        this.appendDummyInput()
            .appendField('Rotate in Arch with Radius');
        this.appendValueInput('R');
        this.appendDummyInput()
            .appendField('Backward Continuously');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('enables the robot to rotate move in arc ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['movArcForward'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(210);
        this.appendDummyInput()
            .appendField('Rotate in Arch with Radius');
        this.appendValueInput('R');
        this.appendDummyInput()
            .appendField('Forward Continuously');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('enables the robot to Move forward with specific radius');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['move'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(160);
        var textInput = new Blockly.Field('speed');
        //input.setCheck(Number);
        this.appendDummyInput()
            .appendField('move Forward')
            .appendField('Speed')
            .appendField(textInput, 'Speed');

        this.setTooltip('enables the robot to move forward');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks['moveBackward'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(210);
        this.appendDummyInput()
            .appendField('Move Backward Continuously');
        this.setTooltip('enables the robot to move Backward forever ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks['moveForward'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(212);
        this.appendDummyInput()
            .appendField('Move Forward Continuously');
        this.setTooltip('enables the robot to move forward forever ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['rotate'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(120);
        this.appendDummyInput()
            .appendField('Rotate with Angle');
        this.appendValueInput('A');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip('enables the robot to rotate with specified angle');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['rotateClockwise'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(210);
        this.appendDummyInput()
            .appendField('Rotate Counterclockwise Continuously');
        this.setTooltip('enables the robot to rotate right forever ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['rotateCounterclockwise'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(210);
        this.appendDummyInput()
            .appendField('Rotate Counterclockwise Continuously');
        this.setTooltip('enables the robot to Rotate Left forever ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['stop'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(0);
        this.appendDummyInput()
            .appendField('Stop');
        this.setTooltip('Stop The motion of the robot ');
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};

Blockly.Blocks['travel'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(low_level_translation_color);
        this.appendDummyInput()
            .appendField(new Blockly.FieldImage("assets/blocks/move_distance.png", 150, 20, "move distance"));
        this.appendValueInput('D');
        this.setInputsInline(true);
        var thisBlock = this;
        this.setTooltip(Blockly.Msg.TRAVEL_TOOLTIP);
        this.setPreviousStatement(true);
        this.setNextStatement(true);

    }
};


Blockly.Blocks['black'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.BLACK_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.BLACK_TOOLTIP);
    }
};

Blockly.Blocks['red'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.RED_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.RED_TOOLTIP);
    }
};

Blockly.Blocks['white'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.WHITE_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.WHITE_TOOLTIP);
    }
};

Blockly.Blocks['green'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.GREEN_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.GREEN_TOOLTIP);
    }
};

Blockly.Blocks['blue'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.BLUE_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.BLUE_TOOLTIP);
    }
};

Blockly.Blocks['brown'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.BROWN_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.BROWN_TOOLTIP);
    }
};

Blockly.Blocks['yellow'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.YELLOW_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.YELLOW_TOOLTIP);
    }
};

Blockly.Blocks['cracked_ice'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.CRACKED_ICE_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.CRACKED_ICE_TOOLTIP);
    }
};

Blockly.Blocks['thin_ice'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.THIN_ICE_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.THIN_ICE_TOOLTIP);
    }
};

Blockly.Blocks['mud'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.MUD_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.MUD_TOOLTIP);
    }
};

Blockly.Blocks['hole'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour(color_sensor_color);
        this.appendDummyInput()
            .appendField(Blockly.Msg.HOLE_TITLE);
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.HOLE_TOOLTIP);
    }
};

Blockly.Blocks['paint_cell_with_color'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/paint.png", 80, 20, "paint"));
    this.appendValueInput("color")
        .setCheck(null);
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/paint_1.png", 100, 18, "color on cell"));
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(user_interaction_color);
    this.setTooltip('Paint current cell with specific color');
  }
};


Blockly.Blocks['print_to_cell'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/set_text.png", 80, 20, "write"));
    this.appendValueInput("text")
        .setCheck(null);
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/set_text_1.png", 61, 18, "on cell"));
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(user_interaction_color);
    this.setTooltip('Print value on the current cell');
  }
};

Blockly.Blocks['wait_for_seconds'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/wait_for_seconds.png", 97, 20, "wait for"));
    this.appendValueInput("seconds")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/wait_for_seconds_1.png", 70, 18, "seconds"));
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(user_interaction_color);
    this.setTooltip('Stops robot at its current position for given seconds');
  }
};

Blockly.Blocks['set_auto_color'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/auto_paint.png", 115 , 20, "auto paint"));
    this.appendValueInput("color")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(user_interaction_color);
    this.setTooltip('Auto paint cells during movement with given color');
  }
};

Blockly.Blocks['drop_item'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/drop_object.png", 125, 20, "drop object"));
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(user_interaction_color);
    this.setTooltip('Drop first item in robot\'s loaded items');
  }
};

Blockly.Blocks['read_from_cell'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/get_text.png", 135, 20, "read cell text"));
    this.setInputsInline(true);
    this.setOutput(true, 'String');
    this.setColour(user_interaction_color);
    this.setTooltip('Read printed character on the current cell');
  }
};

Blockly.Blocks['none_color'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("None");
    this.setInputsInline(true);
    this.setOutput(true, 'String');
    this.setColour(225);
    this.setTooltip('Paint nothing to the cell');
  }
};

Blockly.Blocks['read_number_from_cell'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("assets/blocks/get_number.png", 170, 20, "read cell number"));
    this.setInputsInline(true);
    this.setOutput(true, 'Number');
    this.setColour(user_interaction_color);
    this.setTooltip('Read printed number on the current cell');
  }
};


//move forward and print
Blockly.Blocks['move_forward_and_print'] = {
    init: function() {
        this.appendValueInput("printed")
            .setCheck(null)
            .appendField(new Blockly.FieldImage("assets/blocks/step_forward_and_print.png", 220, 20, "step forward and print"));
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(move_forward_and_print_color);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['a'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            //.appendField(new Blockly.FieldLabel('Styled label', 'style-name'));
            .appendField(new Blockly.FieldLabel('A', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("A");
        this.setShadow(true);
    }
};

Blockly.Blocks['b'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('B', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("B");
        this.setShadow(true);
    }
};

Blockly.Blocks['c'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('C', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("C");
        this.setShadow(true);
    }
};

Blockly.Blocks['d'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('D', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("D");
        this.setShadow(true);
    }
};

Blockly.Blocks['e'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('E', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("E");
        this.setShadow(true);
    }
};

Blockly.Blocks['f'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('F', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("F");
        this.setShadow(true);
    }
};

Blockly.Blocks['g'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('G', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("G");
        this.setShadow(true);
    }
};

Blockly.Blocks['h'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('H', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("H");
        this.setShadow(true);
    }
};

Blockly.Blocks['i'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('I', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("I");
        this.setShadow(true);
    }
};

Blockly.Blocks['j'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('J', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("J");
        this.setShadow(true);
    }
};

Blockly.Blocks['k'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('K', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("K");
        this.setShadow(true);
    }
};

Blockly.Blocks['l'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('L', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("L");
        this.setShadow(true);
    }
};

Blockly.Blocks['m'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('M', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("M");
        this.setShadow(true);
    }
};

Blockly.Blocks['n'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('N', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("N");
        this.setShadow(true);
    }
};

Blockly.Blocks['o'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('O', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("O");
        this.setShadow(true);
    }
};


Blockly.Blocks['p'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('P', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("P");
        this.setShadow(true);
    }
};

Blockly.Blocks['q'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('Q', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("Q");
        this.setShadow(true);
    }
};

Blockly.Blocks['r'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('R', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("R");
        this.setShadow(true);
    }
};

Blockly.Blocks['s'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('S', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("S");
        this.setShadow(true);
    }
};

Blockly.Blocks['t'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('T', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("T");
        this.setShadow(true);
    }
};

Blockly.Blocks['u'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('U', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("U");
        this.setShadow(true);
    }
};

Blockly.Blocks['v'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('V', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("V");
        this.setShadow(true);
    }
};

Blockly.Blocks['w'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('W', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("W");
        this.setShadow(true);
    }
};

Blockly.Blocks['x'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('X', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("X");
        this.setShadow(true);
    }
};

Blockly.Blocks['y'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('Y', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("Y");
        this.setShadow(true);
    }
};

Blockly.Blocks['z'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('Z', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("Z");
        this.setShadow(true);
    }
};

//numbers

Blockly.Blocks['zero'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('0', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("0");
        this.setShadow(true);
    }
};

Blockly.Blocks['one'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('1', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("1");
        this.setShadow(true);
    }
};

Blockly.Blocks['two'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('2', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("2");
        this.setShadow(true);
    }
};

Blockly.Blocks['three'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('3', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("3");
        this.setShadow(true);
    }
};

Blockly.Blocks['four'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('4', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("4");
        this.setShadow(true);
    }
};

Blockly.Blocks['five'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('5', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("5");
        this.setShadow(true);
    }
};

Blockly.Blocks['six'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('6', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("6");
        this.setShadow(true);
    }
};

Blockly.Blocks['seven'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('7', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("7");
        this.setShadow(true);
    }
};

Blockly.Blocks['eight'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('8', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("8");
        this.setShadow(true);
    }
};

Blockly.Blocks['nine'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('9', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("9");
        this.setShadow(true);
    }
};

Blockly.Blocks['ten'] = {
    init: function() {
        this.setHelpUrl(null);
        this.setColour("#e8e8e8");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_CENTRE)
            .appendField(new Blockly.FieldLabel('10', 'letter'));
        this.setOutput(true, 'String');
        this.setTooltip("10");
        this.setShadow(true);
    }
};

//move then return blocks

Blockly.Blocks['move_forward_then_return'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("move forward then return");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(290);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['move_backward_then_return'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("move backward then return");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(290);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['move_right_then_return'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("move right then return");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(290);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['move_left_then_return'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("move left then return");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(290);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};