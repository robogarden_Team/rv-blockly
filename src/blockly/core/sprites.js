'use strict';

goog.provide('Blockly.SpritesCustom');

goog.require('goog.Timer');
goog.require('goog.dom');
goog.require('goog.math');
goog.require('goog.math.Rect');
goog.require('Blockly.Touch');
goog.require('goog.dom');

Blockly.TTRASH = {
    width: 42,
    height: 45,
    url: "trashBig.png",
    yax: 16
};

Blockly.HTRASH = {
    width: 42,
    height: 9,
    url: "trashSmall.png",
    yax: 9
};

Blockly.RESET = {
    width: 32,
    height: 32,
    url: "reset.png",
    yax: 0,
    xax: 0
};

Blockly.MINUS = {
    width: 32,
    height: 32,
    url: "minus.png",
    yax: 77,
    xax: 0
};

Blockly.PLUS = {
    width: 32,
    height: 32,
    url: "plus.png",
    yax: 43,
    xax: 0
};


Blockly.Trashcan.prototype.createDom = function() {

    this.svgGroup_ = Blockly.utils.createSvgElement("g", {
        "class": "blocklyTrash"
    }, null);
    var a = String(Math.random()).substring(2),
        b = Blockly.utils.createSvgElement("clipPath", {
            id: "blocklyTrashBodyClipPath" + a
        }, this.svgGroup_);
    Blockly.utils.createSvgElement("rect", {
        width: this.WIDTH_,
        height: this.BODY_HEIGHT_,
        y: this.LID_HEIGHT_
    }, b);
    Blockly.utils.createSvgElement("image", {
        width: Blockly.TTRASH.width,
        x: -this.SPRITE_LEFT_,
        height: Blockly.TTRASH.height,
        y: Blockly.TTRASH.yax,
        "clip-path": "url(#blocklyTrashBodyClipPath" + a + ")"
    }, this.svgGroup_).setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", this.workspace_.options.pathToMedia + Blockly.TTRASH.url);
    

    b = Blockly.utils.createSvgElement("clipPath", {
        id: "blocklyTrashLidClipPath" + a
    }, this.svgGroup_);
    Blockly.utils.createSvgElement("rect", {
        width: this.WIDTH_,
        height: this.LID_HEIGHT_
    }, b);
    this.svgLid_ = Blockly.utils.createSvgElement("image", {
        width: Blockly.HTRASH.width,
        x: -this.SPRITE_LEFT_,
        height: Blockly.HTRASH.height,
        y: Blockly.HTRASH.yax,
        "clip-path": "url(#blocklyTrashLidClipPath" + a + ")"
    }, this.svgGroup_);
    this.svgLid_.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", this.workspace_.options.pathToMedia + Blockly.HTRASH.url);
    Blockly.bindEventWithChecks_(this.svgGroup_, "mouseup", this, this.click);
    this.animateLid_();
    return this.svgGroup_
};


Blockly.ZoomControls.prototype.createDom = function() {
    var a = this.workspace_;


    this.svgGroup_ = Blockly.utils.createSvgElement("g", {
        "class": "blocklyZoom"
    }, null);
    var b = String(Math.random()).substring(2),
        c = Blockly.utils.createSvgElement("clipPath", {
            id: "blocklyZoomoutClipPath" + b
        }, this.svgGroup_);
    Blockly.utils.createSvgElement("rect", {
        width: 32,
        height: 32,
        y: 77
    }, c);
    var d = Blockly.utils.createSvgElement("image", {
        width: Blockly.MINUS.width,
        height: Blockly.MINUS.height,
        x: Blockly.MINUS.xax,
        y: Blockly.MINUS.yax,
        "clip-path": "url(#blocklyZoomoutClipPath" +
            b + ")"
    }, this.svgGroup_);
    d.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", a.options.pathToMedia + Blockly.MINUS.url);
    

    c = Blockly.utils.createSvgElement("clipPath", {
        id: "blocklyZoominClipPath" + b
    }, this.svgGroup_);
    Blockly.utils.createSvgElement("rect", {
        width: 32,
        height: 32,
        y: 43
    }, c);
    var e = Blockly.utils.createSvgElement("image", {
        width: Blockly.PLUS.width,
        height: Blockly.PLUS.height,
        x: Blockly.PLUS.xax,
        y: Blockly.PLUS.yax,
        "clip-path": "url(#blocklyZoominClipPath" + b + ")"
    }, this.svgGroup_);
    e.setAttributeNS("http://www.w3.org/1999/xlink",
        "xlink:href", a.options.pathToMedia + Blockly.PLUS.url);
    

    c = Blockly.utils.createSvgElement("clipPath", {
        id: "blocklyZoomresetClipPath" + b
    }, this.svgGroup_);
    Blockly.utils.createSvgElement("rect", {
        width: 32,
        height: 32
    }, c);
    b = Blockly.utils.createSvgElement("image", {
        width: Blockly.RESET.width,
        height: Blockly.RESET.height,
        y: Blockly.RESET.yax,
        "clip-path": "url(#blocklyZoomresetClipPath" + b + ")"
    }, this.svgGroup_);
    b.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", a.options.pathToMedia + Blockly.RESET.url);
    

    Blockly.bindEventWithChecks_(b,
        "mousedown", null,
        function(b) {
            a.markFocused();
            a.setScale(a.options.zoomOptions.startScale);
            a.scrollCenter();
            Blockly.Touch.clearTouchIdentifier();
            b.stopPropagation();
            b.preventDefault()
        });
    Blockly.bindEventWithChecks_(e, "mousedown", null, function(b) {
        a.markFocused();
        a.zoomCenter(1);
        Blockly.Touch.clearTouchIdentifier();
        b.stopPropagation();
        b.preventDefault()
    });
    Blockly.bindEventWithChecks_(d, "mousedown", null, function(b) {
        a.markFocused();
        a.zoomCenter(-1);
        Blockly.Touch.clearTouchIdentifier();
        b.stopPropagation();
        b.preventDefault()
    });
    return this.svgGroup_
};