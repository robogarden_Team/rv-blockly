// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');

// Lint Task
gulp.task('lint', function() {
	return gulp.src([
			 "src/blockly/blocks_compressed.js",
			 "src/blockly/python_compressed.js",
			 "src/blockly/javascript_compressed.js",
			 "src/en.js"
		])
		.pipe(jshint({ devel: true }))
		.pipe(jshint.reporter('default'));
});

// Concatenate & Minify THREE JS
gulp.task('dist', function() {
		gulp.src([
			 "src/blockly/blocks_compressed.js",
			 "src/blockly/python_compressed.js",
			 "src/blockly/javascript_compressed.js",
			 "src/en.js"
		])
		.pipe(concat('rv-blockly.js'))
		.pipe(gulp.dest('dist'))
		.pipe(rename('rv-blockly.min.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
		.pipe(gulp.dest('dist'));

    	return gulp.src([
        	'src/blockly/blockicons/**/*'
    	], {
    	}).pipe(gulp.dest('dist/blockicons'));

});

// Watch Files For Changes
gulp.task('watch', function() {
	gulp.watch([
			 "src/blockly/blocks_compressed.js",
			 "src/blockly/python_compressed.js",
			 "src/blockly/javascript_compressed.js",
			 "src/en.js"
	], [
		'lint',
		'dist',
	]);
});

// Default Task
gulp.task('default', [
	'lint',
	'dist',
	'watch'
]);